const model = require("../models/siasn_ref/skpd.model");
const Joi = require("joi");
const arrayToTree = require("array-to-tree");

const schema = Joi.object({
  view: Joi.string().valid("tree", "flat"),
});

module.exports.index = async (req, res) => {
  try {
    const { query } = req;
    const { value, error } = schema.validate(query);

    if (error) {
      const message = error.details.map((d) => d.message).join(",");
      res.boom.badData("parameter query salah", { message });
    } else {
      const result = await model.query().select("id", "pId", "name");
      const { view } = value;

      const data =
        view === "tree"
          ? arrayToTree(result, {
              customID: "id",
              parentProperty: "pId",
            })
          : result;

      res.status(200).json({ code: 200, message: "success", data });
    }
  } catch (e) {
    console.log(e);
    res.boom.badRequest("Internal Server Error");
  }
};

module.exports.create = async (req, res) => {};
module.exports.patch = async (req, res) => {};
module.exports.update = async (req, res) => {};
module.exports.delete = async (req, res) => {};

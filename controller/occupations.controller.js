const Joi = require("joi");
const arrayToTree = require("array-to-tree");

// jabatan pelaksana dan fungsional
const pelaksanaModel = require("../models/siasn_ref/jfu.model");
const fungsionalModel = require("../models/siasn_ref/jft.model");
const strukturalModel = require("../models/siasn_ref/jabatan_struktural.model");

const schema = Joi.object({
  view: Joi.string().valid("tree", "flat").required(),
  type: Joi.string().valid("pelaksana", "fungsional", "struktural").required(),
});

// karena ini merupakan data jabatan yang ada pada siasn_ref maka terdapat jabatan fungsional, dan pelaksana
module.exports.index = async (req, res) => {
  try {
    const { query } = req;
    const { error, value } = schema.validate(query);

    if (error) {
      const message = error.details.map((d) => d.message).join(",");
      res.boom.badData("paramter query salah", { message });
    } else {
      const { view, type } = value;

      let currentData;

      switch (type) {
        case "pelaksana":
          currentData = await pelaksanaModel
            .query()
            .select("id", "pId", "name");
          break;
        case "struktural":
          currentData = await strukturalModel.query();
          break;
        case "fungsional":
          currentData = await fungsionalModel
            .query()
            .select("id", "pId", "name");
          break;

        default:
          break;
      }

      let result;

      if (!type !== "struktural") {
        result =
          view === "tree"
            ? arrayToTree(currentData, {
                customID: "id",
                parentProperty: "pId",
              })
            : currentData;

        res.status(200).json({ code: 200, message: "success", data: result });
      } else {
        result = currentData;
        res.status(200).json({ code: 200, message: "success", data: result });
      }
    }
  } catch (e) {
    console.log(e);
    res.boom.badRequest("Internal Server Error");
  }
};

module.exports.create = async (req, res) => {};
module.exports.patch = async (req, res) => {};
module.exports.update = async (req, res) => {};
module.exports.delete = async (req, res) => {};

/* eslint-disable no-console */

const path = require("path");
const url = require("url");
const rewrite = require("express-urlrewrite");
require("dotenv").config();

const set = require("lodash/set");
const express = require("express"); // eslint-disable-line import/no-unresolved
const helmet = require("helmet");
const cors = require("cors");
const boom = require("express-boom");

const { Provider } = require("oidc-provider");

const Account = require("./support/account");

const configuration = require("./support/configuration");
const routes = require("./routes/express");

const { PORT = 3000, ISSUER = process.env.ISSUER } = process.env;
configuration.findAccount = Account.findAccount;

const app = express();

app.use("/public", express.static(__dirname + "/public"));

app.use(
  helmet({
    // not like this :(
    contentSecurityPolicy: false,
  })
);
app.use(cors());
app.use(boom());

app.set("views", path.join(__dirname, "views"));
app.set("view engine", "ejs");

let server;

(async () => {
  const adapter = require("./adapters/sequelize");
  adapter.connect();

  const prod = process.env.NODE_ENV === "production";

  if (prod) {
    set(configuration, "cookies.short.secure", true);
    set(configuration, "cookies.long.secure", true);
  }

  app.locals.baseUrl = process.env.baseUrl;
  const provider = new Provider(ISSUER, { adapter, ...configuration });

  if (prod) {
    app.enable("trust proxy");
    provider.proxy = true;

    app.use((req, res, next) => {
      if (req.secure) {
        next();
      } else if (req.method === "GET" || req.method === "HEAD") {
        res.redirect(
          url.format({
            protocol: "https",
            host: req.get("host"),
            pathname: req.originalUrl,
          })
        );
      } else {
        res.status(400).json({
          error: "invalid_request",
          error_description: "do yourself a favor and only use https",
        });
      }
    });
  }

  routes(app, provider);

  app.locals.baseUrl = process.env.baseUrl;
  app.use(rewrite("/.well-known/*", "/oidc/.well-known/$1"));
  app.use("/oidc", provider.callback);

  server = app.listen(PORT, () => {
    console.log(
      `application is listening on port ${PORT}, check its /.well-known/openid-configuration`
    );
  });
})().catch((err) => {
  if (server && server.listening) server.close();
  console.error(err);
  process.exitCode = 1;
});

const scopeList = {
  openid: {
    displayName: "openid",
    description: "Request menggunakan openid",
  },
  profile: {
    displayName: "Melihat informasi profile anda",
    description:
      "Melihat data profil mu. Meliputi nama, foto, tempat bekerja, tanggal lahir, pangkat dan jabatan",
  },
  address: {
    displayName: "Melihat alamat anda",
    description:
      "Melihat alamatmu meliputi Nama Jalan, Kecamatan, Kota, Kode Pos, dan Provinsi",
  },
  email: {
    displayName: "Melihat Alamat Email Anda",
    description: "Melihat alamat emailmu",
  },
  phone: {
    displayName: "Melihat Nomer Handphone Anda",
    description: "Melihat nomer ponselmu",
  },
};

module.exports = function (scope) {
  return {
    description: scopeList[scope]["description"],
    displayName: scopeList[scope]["displayName"],
  };
};

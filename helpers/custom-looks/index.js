// ini adalah custom render pages yang disarankan pada framework untuk dilakukan perubahan

const renderError = require("./renderError");
const postLogoutSuccessSource = require("./postLogoutSuccessSource");
const successSource = require("./successSource");
const userCodeConfirmSource = require("./userCodeConfirmSource");
const userCodeInputSource = require("./userCodeInputSource");
const logoutSource = require("./logoutSource");

module.exports = {
  renderError,
  postLogoutSuccessSource,
  successSource,
  userCodeConfirmSource,
  userCodeInputSource,
  logoutSource,
};

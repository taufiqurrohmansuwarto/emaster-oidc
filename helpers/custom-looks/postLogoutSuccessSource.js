module.exports = async (ctx) => {
  const {
    clientId,
    clientName,
    clientUri,
    initiateLoginUri,
    logoUri,
    policyUri,
    tosUri, // eslint-disable-line no-unused-vars, max-len
  } = ctx.oidc.client || {}; // client is defined if the user chose to stay logged in with the OP
  const display = clientName || clientId;
  ctx.body = `<!DOCTYPE html>
    <head>
      <meta charset="utf-8">
      <title>Sign-out Success</title>
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <meta http-equiv="x-ua-compatible" content="ie=edge">
      <style>
        @import url(https://fonts.googleapis.com/css?family=Roboto:400,100);h1,h1+p{font-weight:100;text-align:center}body{font-family:Roboto,sans-serif;margin-top:25px;margin-bottom:25px}.container{padding:0 40px 10px;width:274px;background-color:#F7F7F7;margin:0 auto 10px;border-radius:2px;box-shadow:0 2px 2px rgba(0,0,0,.3);overflow:hidden}h1{font-size:2.3em}
      </style>
    </head>
    <body>
      <div class="container">
        <h1>Sign-out Success</h1>
        <p>Your sign-out ${display ? `with ${display}` : ""} was successful.</p>
      </div>
    </body>
    </html>`;
};

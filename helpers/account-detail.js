// untuk kebutuhan model
const userModel = require("../models/siasn_master/biodata.model");
const kecamatanModel = require("../models/siasn_ref/kec.model");
const jenisJabatanModel = require("../models/siasn_master/jenis_jabatan.model.js");
const detailSkpd = require("./detail-skpd");

const trim = require("lodash/trim");
const capitalize = require("lodash/capitalize");
const wordCapitalize = (string) => {
  return string
    .split(" ")
    .map((r) => capitalize(r))
    .join(" ");
};

const { parsePhoneNumberFromString } = require("libphonenumber-js");

//  get information about jabtaan,pangkat, golongan dan informasi lainnya yang berhubungan dengan kepegawaian ./.
const rwytJabatanStruktural = require("../models/siasn_master/rwyt_jab_struktural.model");
const rwytJabatanFungsional = require("../models/siasn_master/rwyt_jab_fungsional.model");
const rwytJabatanPelaksana = require("../models/siasn_master/rwyt_jab_pelaksana.model");
const rwytPangkat = require("../models/siasn_master/rwyt_pangkat.model");

const getInformationJabatan = async (pegawaiId) => {
  try {
    const jabatan = await jenisJabatanModel
      .query()
      .select("jabatan_id")
      .where("pegawai_id", pegawaiId)
      .first();

    const pangkatPegawai = await rwytPangkat
      .query()
      .select("kp_id", "pegawai_id")
      .where("pegawai_id", pegawaiId)
      .withGraphJoined("pangkat(profile)")
      .first();

    let result;

    result = {
      ...result,
      pangkat: pangkatPegawai?.pangkat?.pangkat,
      golongan: pangkatPegawai?.pangkat?.gol_ruang,
    };

    // 1. jabatan struktural 2. jabatan fungsional 3.jabatan pelaksana
    switch (parseInt(jabatan?.jabatan_id)) {
      //
      case 1:
        const currentJabatanStruktural = await rwytJabatanStruktural
          .query()
          .select(
            "struktural_id",
            "pegawai_id",
            "eselon_id",
            "siasn_master.rwyt_jab_struktural.jab_struktural_id",
            "instansi",
            "unit_kerja",
            "induk"
          )
          .where("pegawai_id", pegawaiId)
          .withGraphFetched("jabatan_struktural(profile)")
          .andWhere("aktif", "Y")
          .first();

        result = {
          ...result,
          jabatan: currentJabatanStruktural?.jabatan_struktural?.jab_struktural,
          induk: currentJabatanStruktural?.induk,
          occupation_type: "Struktural",
          instansi: currentJabatanStruktural?.instansi,
          unit_kerja: currentJabatanStruktural?.unit_kerja,
        };

        break;
      case 2:
        const currentJabatanFungsional = await rwytJabatanFungsional
          .query()
          .select(
            "fungsional_id",
            "pegawai_id",
            "jft_id",
            "instansi",
            "unit_kerja",
            "induk"
          )
          .where("pegawai_id", pegawaiId)
          .withGraphFetched("jft(profile)")
          .andWhere("aktif", "Y")
          .first();

        result = {
          ...result,
          jabatan: currentJabatanFungsional?.jft.name,
          induk: currentJabatanFungsional?.induk,
          occupation_type: "Fungsional",
          instansi: currentJabatanFungsional?.instansi,
          unit_kerja: currentJabatanFungsional?.unit_kerja,
        };
        break;
      case 3:
        const currentJabatanPelaksana = await rwytJabatanPelaksana
          .query()
          .select(
            "pegawai_id",
            "pelaksana_id",
            "jfu_id",
            "instansi",
            "unit_kerja",
            "induk"
          )
          .where("pegawai_id", pegawaiId)
          .withGraphFetched("jfu(profile)")
          .andWhere("aktif", "Y")
          .first();

        result = {
          ...result,
          jabatan: currentJabatanPelaksana?.jfu?.name,
          induk: currentJabatanPelaksana?.induk,
          occupation_type: "Pelaksana",
          instansi: currentJabatanPelaksana?.instansi,
          unit_kerja: currentJabatanPelaksana?.unit_kerja,
        };

        break;
      default:
        return;
    }

    return result;
  } catch (e) {
    console.log(e);
  }
};

module.exports.detail = async (pegawaiId) => {
  let result;

  try {
    const user = await userModel
      .query()
      .select(
        "tgl_lahir",
        "gelar_depan",
        "gelar_belakang",
        "jk",
        "nip_baru",
        "email",
        "nama",
        "no_hp",
        "alamat",
        "kode_pos",
        "jam_edit_biodata",
        "tgl_edit_biodata",
        "skpd_id"
      )
      .where("aktif", "Y")
      .where("pegawai_id", pegawaiId)
      .andWhere("blokir", "N")
      .withGraphFetched("[fileDiri]")
      .modifyGraph("fileDiri", (builder) => {
        builder.select("file_foto");
      })
      .first();

    if (user) {
      // alamat
      const [
        jalan,
        rt,
        rw,
        desa,
        provinsi,
        kota,
        kecamatan,
      ] = user.alamat.split("|");

      const alamat = await kecamatanModel
        .query()
        .where("id_kec", kecamatan)
        .select("nama_kec")
        .withGraphFetched("[kabupaten, provinsi]")
        .modifyGraph("[kabupaten]", (builder) => {
          builder.select("nama_kabkot");
        })
        .modifyGraph("[provinsi]", (builder) => {
          builder.select("nama_prov");
        })
        .first();

      const {
        pangkat,
        golongan,
        jabatan,
        induk,
        instansi,
        unit_kerja,
        occupation_type,
      } = await getInformationJabatan(pegawaiId);

      //   biodata
      const {
        tgl_lahir: birthdate,
        jk: gender,
        email,
        nama: name,
        nama: nickname,
        nama: middlename,
        nama: preferred_username,
        no_hp: phone_number,
        gelar_depan,
        nip_baru: nip,
        gelar_belakang,
        kode_pos: postal_code,
        jam_edit_biodata,
        tgl_edit_biodata,
        skpd_id,
      } = user;

      const opdSaatIni = await detailSkpd(skpd_id);

      result = {
        address: {
          formatted: `${jalan}, RT ${rt} RW ${rw}, KECAMATAN ${alamat?.nama_kec}, KABUPATEN ${alamat?.kabupaten?.nama_kabkot}, PROVINSI ${alamat?.provinsi.nama_prov}`,
          country: "Indonesia",
          postal_code,
          locality: alamat?.kabupaten?.nama_kabkot,
          region: alamat?.provinsi?.nama_prov,
          street_address: `${jalan}`,
        },
        employee_number: nip,
        range: `${golongan} - ${pangkat}`,
        occupation: jabatan,
        government_agencies: `${induk} - ${instansi} - ${unit_kerja}`,
        birthdate,
        occupation_type,
        gender: gender === "L" ? "male" : "female",
        email: email || `${nama}@master.jatimprov.go.id`,
        email_verified: email ? true : false,
        locale: "id-ID",
        name: trim(`${gelar_depan} ${name} ${gelar_belakang}`),
        nickname: wordCapitalize(nickname),
        middlename,
        preferred_username,
        website: "https://master.bkd.jatimprov.go.id",
        phone_number:
          parsePhoneNumberFromString(phone_number, "ID").number || "0000000",
        phone_number_verified: phone_number ? true : false,
        profile: `https://master.bkd.jatimprov.go.id/files_jatimprov/${user.fileDiri.file_foto}`,
        picture: `https://master.bkd.jatimprov.go.id/files_jatimprov/${user.fileDiri.file_foto}`,
        updated_at: `${tgl_edit_biodata} ${jam_edit_biodata}`,
        current_government_agencies: opdSaatIni,
      };
    }

    return result;
  } catch (error) {
    console.log(error);
  }
};

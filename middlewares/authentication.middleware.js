module.exports.getTokenFromRequestHeader = (req, res, next) => {
  const token = req.get("Authorization");
  if (!token) {
    res.append("WWW-Authenticate", "Basic realm=401").status(401).send();
  } else {
    const matches = token.match(/Bearer\s(\S+)/);
    if (!matches) {
      res.append("WWW-Authenticate", "401 Not Authorized").status(401).send();
    } else {
      res.locals.token = matches[1];
      next();
    }
  }
};

module.exports.isClientCredentials = (req, res, next) => {
  const { token } = res.locals;
  if (token.isClientCredentials) {
    next();
  } else {
    res.append("WWW-Authenticate", "Invalid Grant types").status(403).json({
      error_type: "invalid grant types",
      error_description: "grant types must be client_credentials",
    });
  }
};

module.exports.getAccessToken = (provider) => {
  return async (req, res, next) => {
    const { token } = res.locals;
    const result = await provider.AccessToken.find(token);
    const hasil = await provider.ClientCredentials.find(token);
    if (!result) {
      if (!hasil) {
        res.status(401).json({
          error_type: "invalid token",
          error_description: "Invalid token : access token is invalid",
        });
      } else {
        const client = await provider.Client.find(hasil.clientId);
        const checkClientCredentilas = client.grantTypes.every(
          (s) => s === "client_credentials"
        );

        res.locals.token = {
          exp: hasil.exp,
          scope: client.scope,
          iat: hasil.iat,
          clientId: client.clientId,
          isClientCredentials: checkClientCredentilas,
        };

        next();
      }
    } else {
      res.locals.token = {
        exp: result.exp,
        scope: result.scope,
        accountId: result.accountId,
        iat: result.iat,
      };

      next();
    }
  };
};

module.exports.validateAccessToken = (req, res, next) => {
  const now = Math.floor(Date.now() / 1000);
  const {
    token: { iat, exp },
  } = res.locals;

  if (iat <= now) {
    if (exp >= now) {
      next();
    }
  } else {
    res.status(401).json({
      error_type: "invalid token",
      error_description: "Invalid token : access token is invalid",
    });
  }
};

module.exports.validateScope = (expectedScopes, options) => {
  if (!Array.isArray(expectedScopes)) {
    throw new Error("paramater expected scopes must be an array");
  }
  return async (req, res, next) => {
    const error = (res) => {
      const err_message = "Insufficient scope";

      if (options && options.failWithError) {
        return next({
          statusCode: 403,
          error: "Forbidden",
          message: err_message,
        });
      }

      res
        .append(
          "WWW-Authenticate",
          `Bearer scope="${expectedScopes.join(" ")}", error=${err_message}`
        )
        .status(403)
        .send(err_message);
    };

    if (expectedScopes.length === 0) {
      return next();
    }

    const clientScope = res.locals.token.scope.split(" ");
    let allowed;
    if (options && options.checkAllScopes) {
      allowed = clientScope.every((scope) => expectedScopes.includes(scope));
    } else {
      allowed = clientScope.some((scope) => expectedScopes.includes(scope));
    }

    return allowed ? next() : error(res);
  };
};

const { development } = require("../.knexfile");

const Knex = require("knex");
module.exports = Knex(development);

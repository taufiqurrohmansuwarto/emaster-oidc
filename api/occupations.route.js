const express = require("express");
const router = express.Router();

const positionController = require("../controller/occupations.controller");
const notImplemented = require("../middlewares/not-implemented");

router.route("/").get(positionController.index).all(notImplemented);

module.exports = router;

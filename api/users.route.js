const express = require("express");
const notImplemented = require("../middlewares/not-implemented");
const router = express.Router();

router.route("/").all(notImplemented);

module.exports = router;

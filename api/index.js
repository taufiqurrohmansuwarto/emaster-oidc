const express = require("express");
const router = express.Router();

const departmentRoute = require("./agencies.route");
const positionRoute = require("./occupations.route");

// setup middleware
const {
  validateScope,
  isClientCredentials: clientCredentials,
} = require("../middlewares/authentication.middleware");

// list of routings
router.use(
  "/agencies",
  [clientCredentials, validateScope(["read:agencies"])],
  departmentRoute
);

router.use(
  "/occupations",
  [clientCredentials, validateScope(["read:occupations"])],
  positionRoute
);

const notFound = (_, res) => {
  res.boom.notFound("Not Found");
};

router.use(notFound);

module.exports = router;

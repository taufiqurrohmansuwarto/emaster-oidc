const express = require("express");
const router = express.Router();
const skpdController = require("../controller/agencies.controller");
const notImplemented = require("../middlewares/not-implemented");

router.route("/").get(skpdController.index).all(notImplemented);

module.exports = router;

require("dotenv").config();

const user = process.env.master_username;
const password = process.env.master_password;
const database = process.env.master_name;
const port = process.env.master_port;
const host = process.env.master_host;

const connection = {
  database,
  user,
  password,
  port,
  host,
};

module.exports = {
  siasn: {
    client: "mysql",
    connection,
    pool: {
      min: 2,
      max: 10,
    },
    migrations: {
      tableName: "knex_migrations",
    },
  },
  development: {
    client: "mysql",
    connection,
    pool: {
      min: 2,
      max: 10,
    },
    migrations: {
      tableName: "knex_migrations",
    },
  },
  staging: {
    client: "mysql",
    connection,
    pool: {
      min: 2,
      max: 10,
    },
    migrations: {
      tableName: "knex_migrations",
    },
  },
  production: {
    client: "mysql",
    connection,
    pool: {
      min: 10,
      max: 50,
    },
  },
};

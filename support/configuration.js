const {
  interactionPolicy: { Prompt, base: policy },
} = require("oidc-provider"); // require('oidc-provider');
const { renderError } = require("../helpers/custom-looks");

require("dotenv").config();

// 302100419881120146599

// copies the default policy, already has login and consent prompt policies
const interactions = policy();

// create a requestable prompt with no implicit checks
const selectAccount = new Prompt({
  name: "select_account",
  requestable: true,
});

const tokenEndpointAuthMethods = [
  "none",
  "client_secret_basic",
  "client_secret_jwt",
  "client_secret_post",
  "private_key_jwt",
];

const jwks = require("../src/jwks.json");

// add to index 0, order goes select_account > login > consent
interactions.add(selectAccount, 0);

module.exports = {
  clients: [
    {
      client_id: "presensi",
      client_secret: "presensi",
      grant_types: ["authorization_code", "refresh_token"],
      redirect_uris: ["http://localhost:8000/callback"],
      scope: "employee_profile openid offline_access",
      response_types: ["code"],
      client_name: "Vendor Aplikasi Presensi Jatim",
    },
    {
      client_id: "development",
      scope: "profile openid offline_access",
      redirect_uris: ["host.exp.exponent://oauthredirect"],
      response_types: ["code"],
      token_endpoint_auth_method: "none",
      client_name: "mobiles",
      contacts: ["taufiqurrohman.suwarto@gmail.com"],
      application_type: "native",
    },
    {
      client_id: "presensi2",
      client_secret: "presensi2",
      application_type: "web",
      backchannel_logout_session_required: true,
      frontchannel_logout_session_required: true,
      token_endpoint_auth_method: "client_secret_basic",
      require_auth_time: false,
      subject_type: "public",
      introspection_endpoint_auth_method: "client_secret_basic",
      revocation_endpoint_auth_method: "client_secret_basic",
      grant_types: ["authorization_code", "refresh_token"],
      redirect_uris: [
        "http://localhost:8000/callback",
        "http://localhost:3000/api/auth/callback/pttpk",
        "https://ptt-nextjs-dsjyb7p6w.vercel.app/api/auth/callback/pttpk",
        "https://ptt-nextjs.vercel.app/api/auth/callback/pttpk",
        "https://ptt-nextjs-go7bowx86.vercel.app/api/auth/callback/pttpk",
        "https://ptt-nextjs-pphb2sin7.vercel.app/api/auth/callback/pttpk",
      ],
      post_logout_redirect_uris: ["http://localhost:3000"],
      scope: "openid profile email address offline_access",
      response_types: ["code"],
      client_name: "Tokodipea",
      logo_uri: "https://i.ibb.co/pJJcm21/download.png",
    },
    {
      client_id: "vendor",
      client_secret: "vendor",
      grant_types: ["client_credentials"],
      redirect_uris: [],
      scope: "openid read:occupations read:agencies",
      response_types: [],
      contacts: ["p3dasi.dev@gmail.com"],
      client_name: "Vendor Aplikasi Presensi Jatim",
    },
    {
      client_id: "vendor3",
      client_secret: "vendor3",
      grant_types: ["client_credentials"],
      redirect_uris: [],
      scope: "openid read:dept",
      response_types: [],
      contacts: ["p3dasi.dev@gmail.com"],
      client_name: "Vendor Aplikasi Presensi Jatim",
    },
    ,
  ],
  interactions: {
    policy: interactions,
    url(ctx, interaction) {
      // eslint-disable-line no-unused-vars
      return `${process.env.baseUrl}/interaction/${ctx.oidc.uid}`;
    },
  },
  cookies: {
    long: { signed: true, maxAge: 1 * 24 * 60 * 60 * 1000 }, // 1 day in ms
    short: { signed: true },
    keys: [
      process.env.secret_one,
      process.env.secret_two,
      process.env.secret_three,
    ],
  },
  dynamicScopes: [],
  // scopes must be array of object and having properties(display name and the description)
  scopes: [
    // personal information
    "profile",
    "openid",
    "address",
    "email",
    "offline_access",

    // client credentials
    "read:agencies",
    "read:occupations",
  ],
  claims: {
    address: ["address"],
    email: ["email", "email_verified"],
    phone: ["phone_number", "phone_number_verified"],
    profile: [
      // untuk pns
      "employee_number",
      "name",
      "range",
      "occupation",
      "occupation_type",
      "government_agencies",
      "current_government_agencies",
      // end addition to public servants properties
      "birthdate",
      "family_name",
      "gender",
      "given_name",
      "locale",
      "middle_name",
      "nickname",
      "picture",
      "preferred_username",
      "profile",
      "updated_at",
      "website",
      "zoneinfo",
    ],
  },
  features: {
    devInteractions: { enabled: false }, // defaults to true
    deviceFlow: { enabled: true }, // defaults to false
    introspection: { enabled: true }, // defaults to false
    revocation: { enabled: true }, // defaults to false
    clientCredentials: { enabled: true },
    // registration: { enabled: true },
    backchannelLogout: { enabled: true },
    frontchannelLogout: { enabled: true },
  },
  conformIdTokenClaims: false,
  jwks,
  formats: {
    // ClientCredentials: "jwt",
  },
  // should change it
  renderError,
  ttl: {
    AccessToken: 1 * 60 * 60, // 1 hour in seconds
    ClientCredentials: 1 * 60 * 60, // 1 hour in seconds
    AuthorizationCode: 10 * 60, // 10 minutes in seconds
    IdToken: 1 * 60 * 60, // 1 hour in seconds
    DeviceCode: 10 * 60, // 10 minutes in seconds
    RefreshToken: 1 * 24 * 60 * 60, // 1 day in seconds
  },
  tokenEndpointAuthMethods,
};

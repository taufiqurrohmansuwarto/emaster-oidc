const toString = require("lodash/toString");
const crypto = require("crypto");

// master check for account model
const model = require("../models/siasn_master/biodata.model");
const { detail } = require("../helpers/account-detail");
const { trim } = require("lodash");

const md5 = (data) => {
  return crypto.createHash("md5").update(data).digest("hex");
};

class Account {
  static async findByLogin(login, password) {
    const user = await model
      .query()
      .select("pegawai_id", "nip_baru", "password")
      .findOne({ nip_baru: trim(login), aktif: "Y", blokir: "N" });

    if (!user) {
      return undefined;
    } else {
      if (md5(password) === user.password) {
        return {
          accountId: `master|${toString(user.pegawai_id)}`,
        };
      } else {
        return undefined;
      }
    }
  }

  static async findByFederated(provider, claims) {
    console.log("find federated...");
    const id = `${provider}.${claims.sub}`;
    return undefined;
  }

  static async findAccount(ctx, id, token) {
    const [_, currentId] = id.split("|");
    try {
      // const user = await biodataModel
      //   .query()
      //   .where("id_ptt", id)
      //   .first()
      //   .select("id_ptt")
      //   .where("aktif", "Y")
      //   .andWhere("blokir", "N")
      //   .first();

      // if (!user) {
      //   return undefined;
      // }

      // eslint-disable-line no-unused-vars
      // token is a reference to the token used for which a given account is being loaded,
      //   it is undefined in scenarios where account claims are returned from authorization endpoint
      // ctx is the koa request context
      return {
        accountId: toString(id),
        async claims(use, scope) {
          const result = await detail(currentId);
          return {
            sub: toString(id),
            ...result,
          };
        },
      };
    } catch (e) {
      console.log(e);
    }
  }
}

module.exports = Account;

const knex = require("../config/knex");
const { Model } = require("objection");
Model.knex(knex);

class Client extends Model {
  static get tableName() {
    return "Client";
  }
}

module.exports = Client;

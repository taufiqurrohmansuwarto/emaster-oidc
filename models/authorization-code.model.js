const knex = require("../config/knex");
const { Model } = require("objection");
Model.knex(knex);

class AuthorizationCode extends Model {
  static get tableName() {
    return "AuthorizationCode";
  }
}

module.exports = AuthorizationCode;

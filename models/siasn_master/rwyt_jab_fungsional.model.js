const { Model } = require("objection");
const knex = require("../../config/knex-siasn");

Model.knex(knex);

class RiwayatJabatanFungsional extends Model {
  static get tableName() {
    return "siasn_master.rwyt_jab_fungsional";
  }

  static get idColumn() {
    return "fungsional_id";
  }

  static get relationMappings() {
    const jft = require("../siasn_ref/jft.model");
    return {
      jft: {
        relation: Model.BelongsToOneRelation,
        modelClass: jft,
        join: {
          from: "siasn_master.rwyt_jab_fungsional.jft_id",
          to: "siasn_ref.ref_jft.id",
        },
      },
    };
  }
}

module.exports = RiwayatJabatanFungsional;

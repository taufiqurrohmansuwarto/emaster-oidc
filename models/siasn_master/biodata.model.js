const { Model } = require("objection");
const knex = require("../../config/knex-siasn");

Model.knex(knex);

class Biodata extends Model {
  static get tableName() {
    return "siasn_master.biodata";
  }

  static get idColumn() {
    return "pegawai_id";
  }

  static get relationMappings() {
    const fileDiri = require("./file_diri.model");
    return {
      fileDiri: {
        modelClass: fileDiri,
        relation: Model.HasOneRelation,
        join: {
          from: "siasn_master.biodata.pegawai_id",
          to: "siasn_master.file_diri.pegawai_id",
        },
      },
    };
  }

  static get modifiers() {
    return {
      login(builder, username) {
        builder
          .select("nip_baru", "password", "pegawai_id")
          .where("nip_baru", username)
          .andWhere("aktif", "Y")
          .andWhere("blokir", "N")
          .first();
      },
    };
  }
}

module.exports = Biodata;

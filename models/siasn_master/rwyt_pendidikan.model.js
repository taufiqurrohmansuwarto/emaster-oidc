const { Model } = require("objection");
const knex = require("../../config/knex-siasn");

Model.knex(knex);

class RiwayatPendidikan extends Model {
  static get tableName() {
    return "siasn_master.rwyt_pendidikan";
  }

  static get idColumn() {
    return "siasn_master.rwyt_pendidikan.pendidikan_id";
  }

  static get relationMappings() {
    const jenjang = require("../siasn_ref/jenjang.model");
    return {
      jenjang: {
        modelClass: jenjang,
        relation: Model.BelongsToOneRelation,
        join: {
          from: "siasn_master.rwyt_pendidikan.jenjang_id",
          to: "siasn_ref.ref_jenjang.jenjang_id",
        },
      },
    };
  }
}

module.exports = RiwayatPendidikan;

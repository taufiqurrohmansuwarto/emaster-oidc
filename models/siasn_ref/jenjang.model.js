const { Model } = require("objection");
const knex = require("../../config/knex-siasn");

Model.knex(knex);

class Jenjang extends Model {
  static get tableName() {
    return "siasn_ref.ref_jenjang";
  }

  static get idColumn() {
    return "siasn_ref.ref_jenjang.jenjang_id";
  }

  static get modifiers() {}
}

module.exports = Jenjang;

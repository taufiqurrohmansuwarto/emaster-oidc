const { Model } = require("objection");
const knex = require("../../config/knex-siasn");

Model.knex(knex);

class JabatanFungsionalUmum extends Model {
  static get tableName() {
    return "siasn_ref.ref_jfu";
  }

  static get idColumn() {
    return "id";
  }

  static get modifiers() {
    return {
      profile(builder) {
        builder.select("id", "name");
      },
    };
  }
}

module.exports = JabatanFungsionalUmum;

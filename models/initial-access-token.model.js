const knex = require("../config/knex");
const { Model } = require("objection");
Model.knex(knex);

class InitialAccessToken extends Model {
  static get tableName() {
    return "InitialAccessToken";
  }
}

module.exports = InitialAccessToken;

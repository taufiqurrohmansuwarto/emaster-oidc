const knex = require("../config/knex");
const { Model } = require("objection");
Model.knex(knex);

class DeviceCode extends Model {
  static get tableName() {
    return "DeviceCode";
  }
}

module.exports = DeviceCode;

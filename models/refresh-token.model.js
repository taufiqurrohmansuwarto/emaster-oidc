const knex = require("../config/knex");
const { Model } = require("objection");
Model.knex(knex);

class RefreshToken extends Model {
  static get tableName() {
    return "RefreshToken";
  }
}

module.exports = RefreshToken;

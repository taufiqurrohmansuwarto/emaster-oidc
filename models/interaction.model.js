const knex = require("../config/knex");
const { Model } = require("objection");
Model.knex(knex);

class Interaction extends Model {
  static get tableName() {
    return "Interaction";
  }
}

module.exports = Interaction;

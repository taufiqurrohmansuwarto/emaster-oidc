/*
 * This is a very rough-edged example, the idea is to still work with the fact that oidc-provider
 * has a rather "dynamic" schema. This example uses sequelize with postgresql, and all dynamic data
 * uses JSON fields. id is set to be the primary key, grantId should be additionaly indexed for
 * models where these fields are set (grantId-able models). userCode should be additionaly indexed
 * for DeviceCode model. uid should be additionaly indexed for Session model.
 */

// npm i sequelize@^5.21.2
const Sequelize = require("sequelize"); // eslint-disable-line import/no-unresolved
require("dotenv").config();

const dbName = process.env.DATABASE_NAME_POSTGRES;
const dbUsername = process.env.DATABASE_USERNAME_POSTGRES;
const host = process.env.DATABASE_HOST_POSTGRES;
const dbPasword = process.env.DATABASE_PASSWORD_POSTGRES;

const dbLocal = process.env.DATABASE_NAME_POSTGRES_LOCAL;
const dbUsernameLocal = process.env.DATABASE_USERNAME_POSTGRES_LOCAL;
const dbPaswordLocal = process.env.DATABASE_PASSWORD_POSTGRES_LOCAL;
const hostLocal = process.env.DATABASE_HOST_POSTGRES_LOCAL;

let sequelize;

// use this setting to host in heroku
const sequelizeHosting = new Sequelize(dbName, dbUsername, dbPasword, {
  host,
  dialect: "postgres",
  logging: false,
  dialectOptions: {
    ssl: {
      require: true,
      rejectUnauthorized: false,
    },
  },
});

if (process.env.is_hosting === "iya") {
  sequelize = sequelizeHosting;
} else {
  sequelize = new Sequelize(dbLocal, dbUsernameLocal, dbPaswordLocal, {
    host: hostLocal,
    dialect: "postgres",
    logging: false,
  });
}

const grantable = new Set([
  "AccessToken",
  "AuthorizationCode",
  "RefreshToken",
  "DeviceCode",
]);

const models = [
  "Session",
  "AccessToken",
  "AuthorizationCode",
  "RefreshToken",
  "DeviceCode",
  "ClientCredentials",
  "Client",
  "InitialAccessToken",
  "RegistrationAccessToken",
  "Interaction",
  "ReplayDetection",
  "PushedAuthorizationRequest",
].reduce((map, name) => {
  map.set(
    name,
    sequelize.define(name, {
      id: { type: Sequelize.STRING, primaryKey: true },
      ...(grantable.has(name)
        ? { grantId: { type: Sequelize.STRING } }
        : undefined),
      ...(name === "DeviceCode"
        ? { userCode: { type: Sequelize.STRING } }
        : undefined),
      ...(name === "Session" ? { uid: { type: Sequelize.STRING } } : undefined),
      data: { type: Sequelize.JSONB },
      expiresAt: { type: Sequelize.DATE },
      consumedAt: { type: Sequelize.DATE },
    })
  );

  return map;
}, new Map());

class SequelizeAdapter {
  constructor(name) {
    this.model = models.get(name);
    this.name = name;
  }

  async upsert(id, data, expiresIn) {
    await this.model.upsert({
      id,
      data,
      ...(data.grantId ? { grantId: data.grantId } : undefined),
      ...(data.userCode ? { userCode: data.userCode } : undefined),
      ...(data.uid ? { uid: data.uid } : undefined),
      ...(expiresIn
        ? { expiresAt: new Date(Date.now() + expiresIn * 1000) }
        : undefined),
    });
  }

  async find(id) {
    const found = await this.model.findByPk(id);
    if (!found) return undefined;
    return {
      ...found.data,
      ...(found.consumedAt ? { consumed: true } : undefined),
    };
  }

  async findByUserCode(userCode) {
    const found = await this.model.findOne({ where: { userCode } });
    if (!found) return undefined;
    return {
      ...found.data,
      ...(found.consumedAt ? { consumed: true } : undefined),
    };
  }

  async findByUid(uid) {
    const found = await this.model.findOne({ where: { uid } });
    if (!found) return undefined;
    return {
      ...found.data,
      ...(found.consumedAt ? { consumed: true } : undefined),
    };
  }

  async destroy(id) {
    await this.model.destroy({ where: { id } });
  }

  async consume(id) {
    await this.model.update({ consumedAt: new Date() }, { where: { id } });
  }

  async revokeByGrantId(grantId) {
    await this.model.destroy({ where: { grantId } });
  }

  static async connect() {
    return sequelize.sync();
  }
}

module.exports = SequelizeAdapter;
